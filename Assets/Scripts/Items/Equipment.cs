﻿using System;

[System.Serializable]
public class Equipment
{
    public EquipmentData equipmentData;
    public string equipmentDataID;
    public string equipmentGUID;
    public StatData hpExtraStat = new StatData(StatusType.HP);
    public StatData strExtraStat = new StatData(StatusType.STR);
    public StatData agiExtraStat = new StatData(StatusType.AGI);
    public StatData intExtraStat = new StatData(StatusType.INT);
    public StatData lukExtraStat = new StatData(StatusType.LUK);
    public int upgradeCountLeft;

    public Equipment(EquipmentData equipmentData)
    {
        this.equipmentData = equipmentData;
        if (equipmentData != null)
        {
            this.equipmentDataID = equipmentData.dataID;
            this.upgradeCountLeft = equipmentData.upgradeCount;
            this.equipmentGUID = Guid.NewGuid().ToString();
        }
    }

    // ADD FUNCTION TO DEAL WITH EXTRA STAT AND UPGRADECOUNT
}
