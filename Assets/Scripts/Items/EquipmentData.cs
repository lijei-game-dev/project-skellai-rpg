﻿using UnityEngine;

public enum EquipmentType { Head, Body, Leg, Arm, Weapon, Wing, Ring, Tail };

[CreateAssetMenu]
public class EquipmentData : ScriptableObject
{
    [Header("Database ID (Remember to add in Database SOs)")]
    public string dataID;
    [Header("Equipment Data")]
    public string equipmentName;
    public RarityType rarityType;
    public EquipmentType equipmentType;
    public float sellingPrice;
    public GameObject equipmentModel;
    public Sprite equipmentIcon;
    // USE AssetReference for Sprite and GameObject to minimize load time
    // public AssetReference test;
    public StatData hpStat = new StatData(StatusType.HP);
    public StatData strStat = new StatData(StatusType.STR);
    public StatData agiStat = new StatData(StatusType.AGI);
    public StatData intStat = new StatData(StatusType.INT);
    public StatData lukStat = new StatData(StatusType.LUK);
    public int upgradeCount;
    public bool isCash;
}
