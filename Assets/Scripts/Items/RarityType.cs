﻿using UnityEngine;

public enum RarityType
{
    Common,
    Uncommon,
    Rare,
    Unique,
    Legendary
};

public static class RarityColor
{
    public static Color Type(RarityType type)
    {
        Color result = Color.grey;
        switch (type)
        {
            case RarityType.Common:
                result = Color.grey;
                break;
            case RarityType.Uncommon:
                result = Color.blue;
                break;
            case RarityType.Rare:
                result = Color.green;
                break;
            case RarityType.Unique:
                result = Color.yellow;
                break;
            case RarityType.Legendary:
                result = Color.red;
                break;
        }
        return result;
    }
}