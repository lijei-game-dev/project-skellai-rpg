﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public ParticleSystem projectileCollideEffect;
    public float selfDestructTimer = 5f;
    public float selfDestructTimerAfterCollide = 0f;
    protected Vector3 previousPosition;
    protected LayerMask projectileMask;
    protected float damageValue = 1f;

    void Start()
    {
        previousPosition = transform.position;
        projectileMask = LayerMask.GetMask("Default", "Walkable", "Player", "Enemy");
        Destroy(gameObject, selfDestructTimer);
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        Ray ray = new Ray(previousPosition, transform.position - previousPosition);
        if (Physics.Raycast(ray, out hit, Vector3.Distance(previousPosition, transform.position), projectileMask, QueryTriggerInteraction.Ignore))
        {
            transform.position = hit.point;
            transform.GetComponent<Rigidbody>().isKinematic = true;
            transform.GetComponent<Rigidbody>().velocity = Vector3.zero;

            // Activate collide effect here
            if (projectileCollideEffect != null)
            {
                transform.SetParent(hit.transform);
                ParticleSystem clonedEffect = Instantiate(projectileCollideEffect, hit.point, Quaternion.LookRotation(hit.normal));
            }

            // Deal Damage if target hit has script Status
            if (hit.transform.GetComponent<Status>() != null)
            {
                hit.transform.GetComponent<Status>().ModifyHPStat(damageValue, false);
            }

            Debug.DrawRay(previousPosition, ray.direction * hit.distance, Color.white, 5f);
            Debug.DrawRay(hit.point, hit.normal * hit.distance, Color.yellow, 5f);
            Destroy(gameObject, selfDestructTimerAfterCollide);
        }
        previousPosition = transform.position;
    }

    public void SetDamageValue(float damageValue)
    {
        this.damageValue = damageValue;
    }
}
