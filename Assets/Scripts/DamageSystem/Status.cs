﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum StatusType { HP, STR, AGI, INT, LUK };

public class Status : MonoBehaviour
{
    public FloatReference maxHP;
    public FloatReference invulnerableTime;
    public List<StatData> statList;
    // public List<StatData> elementResistanceList; etc....
    public UnityEvent statChangedEvent;
    StatData HP;
    public bool isInvulnerable { get; set; }
    public float currentHitPoint { get { return HP.amount.value; } }

    void Awake()
    {
        if (statList == null)
        {
            Debug.Log("Must include at least HP stat in Status script!");
            return;
        }

        foreach (StatData statData in statList)
        {
            if (statData.stat == StatusType.HP)
            {
                HP = statData;
                ResetHP();
                break;
            }
        }
    }

    void Update()
    {
        // Invulnerable code here
        // something like this:--
        // if (isInvulnerable)
        // {
        //     m_timeSinceLastHit += Time.deltaTime;
        //     if (m_timeSinceLastHit > invulnerabiltyTime)
        //     {
        //         m_timeSinceLastHit = 0.0f;
        //         isInvulnerable = false;
        //         OnBecomeVulnerable.Invoke();
        //     }
        // }
        if (gameObject.tag != "Player" && HP.amount.value <= 0)
        {
            if (GetComponent<DropLoot>() != null)
            {
                GetComponent<DropLoot>().DropLoots();
            }
            Destroy(gameObject);
        }
    }

    public void ResetHP()
    {
        HP.amount.setValue(maxHP.value);
        isInvulnerable = false;
    }

    public void ModifyStat(List<StatData> statModifierList, bool isIncrease = true)
    {
        foreach (StatData x in statModifierList)
        {
            foreach (StatData currentStat in statList)
            {
                if (currentStat.stat == x.stat)
                {
                    currentStat.amount.setValue(isIncrease ? currentStat.amount.value + x.amount.value : currentStat.amount.value - x.amount.value);
                    break;
                }
            }
        }
        statChangedEvent.Invoke();
    }

    public void ModifyStat(StatData statModifier, bool isIncrease = true)
    {
        foreach (StatData currentStat in statList)
        {
            if (currentStat.stat == statModifier.stat)
            {
                currentStat.amount.setValue(isIncrease ? currentStat.amount.value + statModifier.amount.value : currentStat.amount.value - statModifier.amount.value);
                break;
            }
        }
        statChangedEvent.Invoke();
    }

    // To reduce running time
    // Using ModifyStat will increase running time 
    public void ModifyHPStat(float damageValue, bool isIncrease = true)
    {
        if (isInvulnerable && !isIncrease)
        {
            return;
        }
        HP.amount.setValue(isIncrease ? HP.amount.value + damageValue : HP.amount.value - damageValue);
        if (HP.amount.value < 0) { HP.amount.setValue(0f); }
        if (HP.amount.value > maxHP.value) { HP.amount.setValue(maxHP.value); }
        statChangedEvent.Invoke();
    }
}
