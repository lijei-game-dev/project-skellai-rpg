﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealZone : MonoBehaviour
{
    public FloatReference HPHealValue;
    [Tooltip("Do not include HP here")]
    public List<StatData> extraHealValue;
    public FloatReference healRate;
    Status status;
    float timer;
    bool onCooldown;

    void Start()
    {
        timer = 0;
        onCooldown = false;
    }

    void Update()
    {
        if (onCooldown)
        {
            timer += Time.deltaTime;
            if (timer > healRate.value)
            {
                timer = 0;
                onCooldown = false;
            }
        }
    }

    void OnTriggerStay(Collider collider)
    {
        status = collider.GetComponent<Status>();

        Heal(status);
    }

    void Heal(Status s)
    {
        if (status == null)
        {
            return;
        }

        if (!onCooldown)
        {
            s.ModifyHPStat(HPHealValue.value, true);
            if (extraHealValue.Count > 0)
            {
                s.ModifyStat(extraHealValue, true);
            }
            Debug.Log("Current health: " + s.currentHitPoint);
            onCooldown = true;
        }
    }
}
