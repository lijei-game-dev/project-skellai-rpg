﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
    public FloatReference HPDamageValue;
    [Tooltip("Do not include HP here")]
    public List<StatData> extraDamageValue;
    public FloatReference damageRate;
    Status status;
    float timer;
    bool onCooldown;

    void Start()
    {
        timer = 0;
        onCooldown = false;
    }

    void Update()
    {
        if (onCooldown)
        {
            timer += Time.deltaTime;
            if (timer > damageRate.value)
            {
                timer = 0;
                onCooldown = false;
            }
        }
    }

    void OnTriggerStay(Collider collider)
    {
        status = collider.GetComponent<Status>();
        if (collider.transform.root != transform.root) { Damage(status); }
    }

    void Damage(Status s)
    {
        if (status == null)
        {
            return;
        }

        if (!onCooldown)
        {
            s.ModifyHPStat(HPDamageValue.value, false);
            if (extraDamageValue.Count > 0)
            {
                s.ModifyStat(extraDamageValue, false);
            }
            Debug.Log("Current health: " + s.currentHitPoint);
            onCooldown = true;
        }
    }
}
