﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusBarUI : MonoBehaviour
{
    private List<StatData> playerStatList;
    Text text;

    void Start()
    {
        text = GetComponent<Text>();
        playerStatList = PlayerManager.Instance.playerStatus.statList;
        ShowStatus();
    }

    public void ShowStatus()
    {
        string hpText = "HP: " + playerStatList[0].amount.value.ToString();
        string strText = "STR: " + playerStatList[1].amount.value.ToString();
        string agiText = "AGI: " + playerStatList[2].amount.value.ToString();
        string intText = "INT: " + playerStatList[3].amount.value.ToString();
        string lukText = "LUK: " + playerStatList[4].amount.value.ToString();
        text.text = hpText + "\n" + strText + "\n" + agiText + "\n" + intText + "\n" + lukText;
    }
}
