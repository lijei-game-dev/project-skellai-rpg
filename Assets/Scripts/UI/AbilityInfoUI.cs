﻿using System.Collections.Generic;
using UnityEngine;

public class AbilityInfoUI : MonoBehaviour
{
    public Transform slotsParent;
    AbilitySlot[] slots;
    List<Ability> playerAbilityList;

    void Awake()
    {
        slots = slotsParent.GetComponentsInChildren<AbilitySlot>();
    }

    void Start()
    {
        playerAbilityList = AbilityManager.Instance.PlayerAbilityList;
        UpdateAbilityInventoryUI();
    }

    public void UpdateAbilityInventoryUI()
    {
        for (int i = 0; i < playerAbilityList.Count; i++)
        {
            if (playerAbilityList[i].abilityData != null)
            {
                slots[i].AddItem(playerAbilityList[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
