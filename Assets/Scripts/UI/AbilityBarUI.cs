﻿using UnityEngine;

public class AbilityBarUI : MonoBehaviour
{
    public Transform slotsParent;
    AbilitySlot[] slots;
    Ability[] playerAbility;

    void Awake()
    {
        slots = slotsParent.GetComponentsInChildren<AbilitySlot>();
    }

    void Start()
    {
        playerAbility = AbilityManager.Instance.PlayerAbility;
        UpdateAbilityBarUI();
    }

    public void UpdateAbilityBarUI()
    {
        for (int i = 0; i < playerAbility.Length; i++)
        {
            if (playerAbility[i].abilityData != null)
            {
                slots[i].AddItem(playerAbility[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
