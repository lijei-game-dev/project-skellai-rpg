﻿using UnityEngine;
using UnityEngine.UI;

// Require either rigidBody or characterController too!
[RequireComponent(typeof(Status))]
public class HealthBarUI : MonoBehaviour
{
    public GameObject healthBarUI;
    public Slider slider;
    FloatReference currentHP;
    FloatReference maxHP;
    Status status;

    // Start is called before the first frame update
    void Start()
    {
        status = GetComponent<Status>();
        maxHP = status.maxHP;
        foreach (StatData statData in status.statList)
        {
            if (statData.stat == StatusType.HP)
            {
                currentHP = statData.amount;
            }
        }
        UpdateHealthBar();
    }

    float CalculateHealthPercentage()
    {
        return currentHP.value / maxHP.value;
    }

    public void UpdateHealthBar()
    {
        slider.value = CalculateHealthPercentage();
        healthBarUI.SetActive(currentHP.value < maxHP.value ? true : false);
    }
}
