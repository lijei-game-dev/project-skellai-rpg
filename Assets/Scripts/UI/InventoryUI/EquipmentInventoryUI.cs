﻿using System.Collections.Generic;

public class EquipmentInventoryUI : InventoryUI
{
    EquipmentSlot[] slots;
    List<Equipment> playerEquipmentInventory;

    void Awake()
    {
        slots = slotsParent.GetComponentsInChildren<EquipmentSlot>();
        playerEquipmentInventory = PlayerManager.Instance.GetPlayerEquipmentInventory();
    }

    public void UpdateEquipmentInventoryUI()
    {
        for (int i = 0; i < PlayerManager.Instance.playerEquipmentInventorySize; i++)
        {
            if (playerEquipmentInventory[i].equipmentData != null)
            {
                slots[i].AddItem(playerEquipmentInventory[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
