﻿using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    public float rotationSpeed = 10f;
    public float distance = 30f;
    Transform thirdPersonCamera;
    Transform cameraLookAt;
    Quaternion rotation;
    Vector3 direction;
    Vector3 pointToLookAt;

    void Start()
    {
        thirdPersonCamera = PlayerManager.Instance.playerThirdPersonCameraRef;
        cameraLookAt = PlayerManager.Instance.playerLookPosRef;
    }

    void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        direction = thirdPersonCamera.position - cameraLookAt.position;
        pointToLookAt = (cameraLookAt.position + direction + direction.normalized * distance) - transform.position;
        rotation = Quaternion.LookRotation(pointToLookAt.normalized);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationSpeed);
        // Debug.DrawLine(transform.position, pointToLookAt, Color.red, Mathf.Infinity);
    }
}
