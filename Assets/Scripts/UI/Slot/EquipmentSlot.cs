﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EquipmentSlot : InventorySlot
{
    public Image frame;
    public GameObject descriptionPanel;
    public TMP_Text descriptionText;
    public bool isInventory = true;
    Equipment equipment;
    bool isHovering = false;
    float panelWidth;
    float panelHeight;

    void Start()
    {
        panelWidth = descriptionPanel.GetComponent<RectTransform>().rect.width;
        panelHeight = descriptionPanel.GetComponent<RectTransform>().rect.height;
    }

    void Update()
    {
        if (isHovering)
        {
            GetPanelPosition();
        }
    }

    public void AddItem(Equipment equipment)
    {
        this.equipment = equipment;
        this.icon.sprite = equipment.equipmentData.equipmentIcon;
        this.icon.enabled = true;
        this.frame.color = RarityColor.Type(equipment.equipmentData.rarityType);
        this.frame.enabled = true;
    }

    public void ClearSlot()
    {
        equipment = null;
        icon.sprite = null;
        icon.enabled = false;
        this.frame.color = RarityColor.Type(RarityType.Common);
        this.frame.enabled = false;
    }

    void GetPanelPosition()
    {
        if (Screen.width - Input.mousePosition.x < panelWidth &&
        Input.mousePosition.y < panelHeight)
        {
            descriptionPanel.gameObject.transform.position = new Vector3(Screen.width - panelWidth, panelHeight);

        }
        else if (Input.mousePosition.y < panelHeight)
        {
            descriptionPanel.gameObject.transform.position = new Vector3(Input.mousePosition.x, panelHeight);

        }
        else if (Screen.width - Input.mousePosition.x < panelWidth)
        {
            descriptionPanel.gameObject.transform.position = new Vector3(Screen.width - panelWidth, Input.mousePosition.y);
        }
        else
        {
            descriptionPanel.gameObject.transform.position = Input.mousePosition;

        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (equipment != null)
        {
            isHovering = true;
            descriptionText.text = equipment.equipmentData.equipmentName + "\n"
            + equipment.equipmentData.rarityType + "\n"
            + equipment.equipmentData.equipmentType + "\n";
            descriptionPanel.SetActive(true);
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        isHovering = false;
        descriptionText.text = "";
        descriptionPanel.SetActive(false);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Debug.Log("LEFT");
        }
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            Debug.Log("RIGHT");
            // Equip current item and save it into equippedList
            if (equipment != null)
            {
                if (isInventory)
                {
                    PlayerManager.Instance.EquipEquipment(equipment);
                }
                else
                {
                    PlayerManager.Instance.UnequipEquipment(equipment);
                }
            }
        }
    }
}
