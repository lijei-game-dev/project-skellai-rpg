﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AbilitySlot : InventorySlot
{
    public Image cooldownMask;
    public TMP_Text cooldownText;
    public GameObject descriptionPanel;
    public TMP_Text descriptionText;
    public bool isInventory = true;
    Ability ability;
    bool isHovering = false;
    float panelWidth;
    float panelHeight;

    void Start()
    {
        panelWidth = descriptionPanel.GetComponent<RectTransform>().rect.width;
        panelHeight = descriptionPanel.GetComponent<RectTransform>().rect.height;
    }

    void Update()
    {
        if (isHovering)
        {
            GetPanelPosition();
        }
        UpdateCooldownMask();
    }

    public void AddItem(Ability ability)
    {
        this.ability = ability;
        this.icon.sprite = ability.abilityData.abilityIcon;
        this.icon.enabled = true;
    }

    public void ClearSlot()
    {
        ability = null;
        icon.sprite = null;
        icon.enabled = false;
    }

    void UpdateCooldownMask()
    {
        if (ability != null && !isInventory)
        {
            float cooldownValue = Mathf.Ceil(ability.CooldownLeft);
            cooldownText.text = ability.OnCooldown() ? cooldownValue.ToString() : "";
            cooldownMask.fillAmount = ability.CooldownLeft / ability.abilityCooldown;
        }
        else
        {
            cooldownText.text = "";
            cooldownMask.fillAmount = 0f;
        }
    }

    void GetPanelPosition()
    {
        if (Screen.width - Input.mousePosition.x < panelWidth &&
        Input.mousePosition.y < panelHeight)
        {
            descriptionPanel.gameObject.transform.position = new Vector3(Screen.width - panelWidth, panelHeight);

        }
        else if (Input.mousePosition.y < panelHeight)
        {
            descriptionPanel.gameObject.transform.position = new Vector3(Input.mousePosition.x, panelHeight);

        }
        else if (Screen.width - Input.mousePosition.x < panelWidth)
        {
            descriptionPanel.gameObject.transform.position = new Vector3(Screen.width - panelWidth, Input.mousePosition.y);
        }
        else
        {
            descriptionPanel.gameObject.transform.position = Input.mousePosition;

        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (ability != null)
        {
            isHovering = true;
            descriptionText.text = ability.abilityData.abilityName + "\n"
            + ability.abilityData.requiredLevel + "\n"
            + ability.abilityData.abilityCooldown + "\n";
            descriptionPanel.SetActive(true);
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        isHovering = false;
        descriptionText.text = "";
        descriptionPanel.SetActive(false);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Debug.Log("LEFT");
        }
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            Debug.Log("RIGHT");
            // Equip current item and save it into equippedList
            if (ability != null)
            {
                if (isInventory)
                {
                    AbilityManager.Instance.EquipAbility(ability);
                }
                else
                {
                    AbilityManager.Instance.UnequipAbility(ability);
                }
            }
        }
    }
}
