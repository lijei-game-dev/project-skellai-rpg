﻿using TMPro;
using UnityEngine;

public class DisplayEquipmentDropModelNameUI : MonoBehaviour
{
    public TMP_Text text;
    string equipmentName;

    void Start()
    {
        EquipmentData equipmentData = GetComponent<PickUpItem>().itemData as EquipmentData;
        equipmentName = equipmentData.equipmentName;
        text.text = equipmentName;
        text.color = RarityColor.Type(equipmentData.rarityType);
    }
}
