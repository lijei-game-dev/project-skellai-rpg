﻿using UnityEngine;

public class PlayerEquipmentInfoUI : MonoBehaviour
{
    public Transform slotsParent;
    EquipmentSlot[] slots;
    Equipment[] playerEquipment;

    void Awake()
    {
        slots = slotsParent.GetComponentsInChildren<EquipmentSlot>();
        playerEquipment = PlayerManager.Instance.GetPlayerEquipment();
    }

    public void UpdatePlayerEquipmentInfoUI()
    {
        for (int i = 0; i < playerEquipment.Length; i++)
        {
            if (playerEquipment[i].equipmentData != null)
            {
                slots[i].AddItem(playerEquipment[i]);
            }
            else
            {
                slots[i].ClearSlot();
            }
        }
    }
}
