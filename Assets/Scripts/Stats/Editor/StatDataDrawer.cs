﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(StatData))]
public class StatDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);

        SerializedProperty stat = property.FindPropertyRelative("stat");
        SerializedProperty amount = property.FindPropertyRelative("amount");

        Rect statRect = new Rect(position);
        statRect.width = position.width / 2.5f;
        position.xMin = statRect.xMax;

        EditorGUI.PropertyField(statRect, stat, GUIContent.none);
        EditorGUI.PropertyField(position, amount, GUIContent.none);

        EditorGUI.EndProperty();
    }
}
