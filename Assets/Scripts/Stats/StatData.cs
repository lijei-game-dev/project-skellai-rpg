﻿[System.Serializable]
public class StatData
{
    public StatusType stat;
    public FloatReference amount;

    public StatData(StatusType stat)
    {
        this.stat = stat;
    }
}
