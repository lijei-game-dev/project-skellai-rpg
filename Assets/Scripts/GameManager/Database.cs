﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Database : ScriptableObject
{
    public List<EquipmentData> equipmentDatabase;
    // other List<> for consumable, etc
}
