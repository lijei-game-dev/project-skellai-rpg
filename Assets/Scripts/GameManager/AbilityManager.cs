﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AbilityManager : MonoBehaviour
{
    private static AbilityManager instance;
    public static AbilityManager Instance { get { return instance; } }
    private List<Ability> playerAbilityList = new List<Ability>();
    private List<Ability> abilityCooldownList = new List<Ability>();
    private static int playerAbilitySize = 10;
    private Ability[] playerAbility = new Ability[playerAbilitySize];

    public int PlayerAbilitySize { get { return playerAbilitySize; } }
    public List<Ability> PlayerAbilityList { get { return playerAbilityList; } }
    public Ability[] PlayerAbility { get { return playerAbility; } }
    public UnityEvent onAbilityBarUpdatedEvent;

    public AbilityData testing;
    public AbilityData testProjectile;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        InitializePlayerAbility();
        playerAbilityList.Add(new Ability(testing));
        playerAbilityList.Add(new Ability(testProjectile));
        playerAbility[0] = playerAbilityList[0];
        playerAbility[1] = playerAbilityList[1];
    }

    void Start()
    {
    }

    void Update()
    {
        if (abilityCooldownList.Count == 0) { return; }

        for (int i = abilityCooldownList.Count - 1; i >= 0; i--)
        {
            if (abilityCooldownList[i].OnCooldown())
            {
                abilityCooldownList[i].Cooldown(Time.deltaTime);
            }
            else
            {
                Debug.Log("Skill " + abilityCooldownList[i].abilityData.abilityName + " is ready to use");
                abilityCooldownList.RemoveAt(i);
            }
        }
    }

    public void ExecuteAbility(int abilitySlot, PlayerPositionReference ppr)
    {
        if (playerAbility[abilitySlot - 1].AbilityReadyToExecute)
        {
            playerAbility[abilitySlot - 1].Execute(ppr);
            if (playerAbility[abilitySlot - 1].HasCooldown())
            {
                abilityCooldownList.Add(playerAbility[abilitySlot - 1]);
            }
        }
        else
        {
            if (playerAbility[abilitySlot - 1].abilityData != null)
                Debug.Log("Ability not ready to use yet! Remaining seconds: " + playerAbility[abilitySlot - 1].CooldownLeft);
        }
    }

    public void EquipAbility(Ability ability)
    {
        if (System.Array.Find(playerAbility, a => a.abilityData == ability.abilityData) != null)
        {
            Debug.Log("Selected ability already in ability bar!");
            return;
        }

        for (int i = 0; i < playerAbilitySize; i++)
        {
            if (playerAbility[i].abilityData == null)
            {
                Debug.Log("Success to equip ability");
                playerAbility[i] = ability;
                onAbilityBarUpdatedEvent.Invoke();
                break;
            }
        }
    }

    public void UnequipAbility(Ability ability)
    {
        for (int i = 0; i < playerAbilitySize; i++)
        {
            if (playerAbility[i].abilityData == ability.abilityData)
            {
                Debug.Log("Success to unequip ability");
                playerAbility[i] = new Ability(null);
                onAbilityBarUpdatedEvent.Invoke();
                break;
            }
        }
    }

    void InitializePlayerAbility()
    {
        for (int i = 0; i < playerAbility.Length; i++)
        {
            playerAbility[i] = new Ability(null);
        }
    }
}
