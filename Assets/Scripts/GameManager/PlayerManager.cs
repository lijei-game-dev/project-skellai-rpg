﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerManager : MonoBehaviour
{
    private static PlayerManager instance;
    public static PlayerManager Instance { get { return instance; } }
    public Transform playerLookPosRef;
    public Transform playerThirdPersonCameraRef;
    public Status playerStatus;
    public List<Interaction> interactions = new List<Interaction>();
    #region Inventory Variables
    public int playerEquipmentInventorySize = 5;
    List<Equipment> playerEquipmentInventory = new List<Equipment>();
    // Get inventory, ability, equipment in this manager too!
    public UnityEvent onInventoryUpdatedEvent;
    #endregion
    #region Player Info Variables
    private static int playerEquipmentSize = System.Enum.GetNames(typeof(EquipmentType)).Length;
    public int PlayerEquipmentSize { get { return playerEquipmentSize; } }
    Equipment[] playerEquipment = new Equipment[playerEquipmentSize];
    public UnityEvent onPlayerInfoUpdatedEvent;
    #endregion

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        if (playerStatus == null)
        {
            Debug.Log("No Status script on this gameObject!");
        }
        UpdateEquipmentInventoryCount();
        InitializePlayerEquipment();
    }

    #region Inventory
    #region Equipment
    public bool AddEquipment(Equipment equipment)
    {
        for (int i = 0; i < playerEquipmentInventorySize; i++)
        {
            if (playerEquipmentInventory[i].equipmentData == null)
            {
                Debug.Log("Added to equipmentInventory");
                playerEquipmentInventory[i] = equipment;
                onInventoryUpdatedEvent.Invoke();
                return true;
            }
        }
        Debug.Log("Equipment Inventory is full!");
        return false;
    }

    public void RemoveEquipment(Equipment equipment)
    {
        // DROP EQUIPMENT FROM INVENTORY
        playerEquipmentInventory[playerEquipmentInventory.IndexOf(equipment)] = new Equipment(null);
        onInventoryUpdatedEvent.Invoke();
        Debug.Log("Equipment is removed from inventory");
    }

    public void SellEquipment(Equipment equipment)
    {
        // ADD SELLING PRICE INTO PLAYERCURRENTGOLD
        RemoveEquipment(equipment);
        onInventoryUpdatedEvent.Invoke();
    }

    public void EquipEquipment(Equipment equipment)
    {
        if (playerEquipment[(int)equipment.equipmentData.equipmentType] == null)
        {
            playerEquipment[(int)equipment.equipmentData.equipmentType] = equipment;
            RemoveEquipment(equipment);
            Debug.Log("Equip equipment to empty equipment slot");
        }
        else
        {
            Equipment temp = playerEquipment[(int)equipment.equipmentData.equipmentType];
            int index = playerEquipmentInventory.IndexOf(equipment);
            playerEquipmentInventory[index] = temp;
            playerEquipment[(int)equipment.equipmentData.equipmentType] = equipment;
            Debug.Log("Equip equipment to non-empty equipment slot");
            onInventoryUpdatedEvent.Invoke();
        }
        onPlayerInfoUpdatedEvent.Invoke();
    }

    public void UnequipEquipment(Equipment equipment)
    {
        if (AddEquipment(equipment))
        {
            playerEquipment[(int)equipment.equipmentData.equipmentType] = new Equipment(null);
            Debug.Log("Success : Unequipment equipment");
            onPlayerInfoUpdatedEvent.Invoke();
        }
        else
        {
            Debug.Log("Unable to unequip equipment, inventory is full");
        }
    }

    public void IncreaseEquipmentInventorySize(int increaseSize)
    {
        playerEquipmentInventorySize += increaseSize;
        UpdateEquipmentInventoryCount();
    }

    void UpdateEquipmentInventoryCount()
    {
        if (playerEquipmentInventory.Count < playerEquipmentInventorySize)
        {
            while (playerEquipmentInventorySize > playerEquipmentInventory.Count)
            {
                playerEquipmentInventory.Add(new Equipment(null));
            }
        }
        else
        {
            // TO BE TESTED
            for (int i = playerEquipmentInventorySize; i < playerEquipmentInventory.Count; i++)
            {
                playerEquipmentInventory.RemoveAt(playerEquipmentInventorySize);
            }
        }
    }
    #endregion
    //ADD MORE ITEM SUBTYPE REGION HERE
    #endregion

    #region Interaction
    public int ExecuteFirstInteraction()
    {
        Debug.Log("Interactions: " + PlayerManager.Instance.interactions.Count);
        int value = (int)interactions[0].interactionType;
        interactions[0].interactable.Interact();
        Debug.Log("Interactions: " + PlayerManager.Instance.interactions.Count);
        return value;
    }

    public void AddInteraction(Interactable interactable)
    {
        Debug.Log("Interactions: " + PlayerManager.Instance.interactions.Count);
        Interaction interaction = new Interaction(interactable);
        interactions.Add(interaction);
        Debug.Log("Interactions: " + PlayerManager.Instance.interactions.Count);
    }

    public void RemoveInteraction(Interactable interactable)
    {
        Debug.Log("Interactions: " + PlayerManager.Instance.interactions.Count);
        // Rework this forloop using Array.Find(interactions, interaction => interaction.interactable == interactable)
        // using System;
        foreach (Interaction interaction in PlayerManager.Instance.interactions)
        {
            if (interaction.interactable == interactable)
            {
                PlayerManager.Instance.interactions.Remove(interaction);
                break;
            }
        }
        Debug.Log("Interactions: " + PlayerManager.Instance.interactions.Count);
    }
    #endregion

    #region Save&Load
    public void Save()
    {
        SaveSystem.SavePlayer();
    }

    public void Load()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        playerStatus.maxHP.setValue(data.maxHP);
        for (int i = 0; i < playerStatus.statList.Count; i++)
        {
            playerStatus.statList[i].amount.setValue(data.statArray[i]);
        }

        playerEquipmentInventorySize = data.equipmentInventorySize;
        UpdateEquipmentInventoryCount();
        for (int i = 0; i < playerEquipmentInventorySize; i++)
        {
            playerEquipmentInventory[i] = data.equipmentInventory[i];
            playerEquipmentInventory[i].equipmentData = DatabaseManager.Instance.GetEquipmentData(data.equipmentInventory[i].equipmentDataID);
        }

        for (int i = 0; i < playerEquipmentSize; i++)
        {
            playerEquipment[i] = data.playerEquipment[i];
            playerEquipment[i].equipmentData = DatabaseManager.Instance.GetEquipmentData(data.playerEquipment[i].equipmentDataID);
        }

    }
    #endregion

    #region Getter
    public List<Equipment> GetPlayerEquipmentInventory()
    {
        return playerEquipmentInventory;
    }

    public Equipment[] GetPlayerEquipment()
    {
        return playerEquipment;
    }
    #endregion

    void InitializePlayerEquipment()
    {
        for (int i = 0; i < playerEquipment.Length; i++)
        {
            playerEquipment[i] = new Equipment(null);
        }
    }
}
