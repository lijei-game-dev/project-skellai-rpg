﻿using System.Collections.Generic;
using UnityEngine;

public class DatabaseManager : MonoBehaviour
{
    private static DatabaseManager instance;
    public static DatabaseManager Instance { get { return instance; } }
    public Database database;
    public Dictionary<string, EquipmentData> equipmentDatabase = new Dictionary<string, EquipmentData>();

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        if (database.equipmentDatabase.Count > 0)
        {
            foreach (EquipmentData equipmentData in database.equipmentDatabase)
            {
                equipmentDatabase.Add(equipmentData.dataID, equipmentData);
            }
        }
    }

    public EquipmentData GetEquipmentData(string equipmentDataID)
    {
        EquipmentData temp = null;

        //This is a safer, but slow, method of accessing
        //values in a dictionary.
        if (equipmentDatabase.TryGetValue(equipmentDataID, out temp))
        {
            //success!
            return temp;
        }
        else
        {
            //failure!
            return null;
        }
    }
}
