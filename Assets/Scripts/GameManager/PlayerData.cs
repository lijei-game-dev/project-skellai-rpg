﻿[System.Serializable]
public class PlayerData
{
    public float maxHP;
    public float[] statArray;
    public int equipmentInventorySize;
    public Equipment[] equipmentInventory;
    public Equipment[] playerEquipment;

    public PlayerData()
    {
        #region Status
        maxHP = PlayerManager.Instance.playerStatus.maxHP.value;
        statArray = new float[PlayerManager.Instance.playerStatus.statList.Count];
        for (int i = 0; i < PlayerManager.Instance.playerStatus.statList.Count; i++)
        {
            statArray[i] = PlayerManager.Instance.playerStatus.statList[i].amount.value;
        }
        #endregion

        #region Inventory
        #region EquipmentInventory
        equipmentInventorySize = PlayerManager.Instance.playerEquipmentInventorySize;
        equipmentInventory = new Equipment[equipmentInventorySize];
        for (int i = 0; i < equipmentInventorySize; i++)
        {
            equipmentInventory[i] = PlayerManager.Instance.GetPlayerEquipmentInventory()[i];
        }
        #endregion
        #endregion

        #region Player Info
        #region EquipmentInfo
        playerEquipment = new Equipment[PlayerManager.Instance.PlayerEquipmentSize];
        for (int i = 0; i < PlayerManager.Instance.PlayerEquipmentSize; i++)
        {
            playerEquipment[i] = PlayerManager.Instance.GetPlayerEquipment()[i];
        }
        #endregion
        #endregion
    }
}
