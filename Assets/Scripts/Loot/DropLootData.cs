﻿using UnityEngine;

[System.Serializable]
public class DropLootData
{
    public GameObject dropModel;
    [Range(0f, 100f)]
    public float dropChance = 0f;
}
