﻿using System.Collections.Generic;
using UnityEngine;

public class DropLoot : MonoBehaviour
{
    public List<DropLootData> dropLootList;
    [Header("Vector3 Range for loots force direction")]
    public float xRange = 150f;
    public float yRange = 400f;
    public float zRange = 150f;

    public void DropLoots()
    {
        for (int i = 0; i < dropLootList.Count; i++)
        {
            if (dropLootList[i].dropModel != null)
            {
                if (Random.Range(0f, 100f) < dropLootList[i].dropChance)
                {
                    Debug.Log("JACKPOT");
                    Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                    GameObject temp = Instantiate(dropLootList[i].dropModel, spawnPos, Quaternion.identity);
                    if (temp.GetComponent<Rigidbody>() != null)
                    {
                        temp.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-xRange, xRange), yRange, Random.Range(-zRange, zRange)));
                    }
                }
            }
        }
    }
}
