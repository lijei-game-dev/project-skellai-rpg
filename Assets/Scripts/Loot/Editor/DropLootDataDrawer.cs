﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(DropLootData))]
public class DropLootDataDrawer : PropertyDrawer
{
    SerializedProperty dropChance;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);

        SerializedProperty dropModel = property.FindPropertyRelative("dropModel");
        SerializedProperty dropChance = property.FindPropertyRelative("dropChance");
        this.dropChance = dropChance;

        Rect dropModelRect = new Rect(position);
        dropModelRect.width = position.width / 2.5f;
        position.xMin = dropModelRect.xMax;

        EditorGUI.PropertyField(dropModelRect, dropModel, GUIContent.none);
        EditorGUI.PropertyField(position, dropChance, GUIContent.none);

        EditorGUI.EndProperty();
    }
}
