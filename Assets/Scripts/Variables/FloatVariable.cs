﻿using UnityEngine;

[CreateAssetMenu]
public class FloatVariable : ScriptableObject
{
    public float value;

    public void SetValue(float value)
    {
        this.value = value;
    }

    public void SetValue(FloatVariable value)
    {
        this.value = value.value;
    }

    public void ApplyChange(float amount)
    {
        this.value += amount;
    }

    public void ApplyChange(FloatVariable amount)
    {
        this.value += amount.value;
    }
}
