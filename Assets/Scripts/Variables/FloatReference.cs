﻿[System.Serializable]
public class FloatReference
{
    public bool useConstant = true;
    public float constantValue;
    public FloatVariable variable;

    public float value
    {
        get { return useConstant ? constantValue : variable.value; }
    }

    public void setValue(float value)
    {
        if (useConstant)
        {
            constantValue = value;
        }
        else
        {
            variable.value = value;
        }
    }
}
