﻿using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Ability Behaviour/Spawn Projectile")]
public class SpawnProjectile : AbilityBehaviour
{
    public Rigidbody projectile;
    public float projectileForce = 100f;
    public FloatReference aimRange;
    public float extraRange = 0f;
    public float[] damage = { 1 };

    public override void Execute(PlayerPositionReference ppr, int abilityLevel)
    {
        Rigidbody clonedProjectile = Instantiate(projectile, ppr.shootPoint.position, projectile.transform.rotation);
        clonedProjectile.transform.Rotate(Vector3.up, ppr.parent.eulerAngles.y, Space.World);
        clonedProjectile.GetComponent<Projectile>().SetDamageValue(damage[abilityLevel - 1]);

        LayerMask projectileMask = LayerMask.GetMask("Default", "Walkable", "Player", "Enemy");
        RaycastHit hit;
        Ray ray = new Ray(ppr.lookPos.position, ppr.mainCamera.forward);
        if (Physics.Raycast(ray, out hit, aimRange.value + extraRange, projectileMask, QueryTriggerInteraction.Ignore))
        {
            clonedProjectile.AddForce((hit.point - ppr.shootPoint.position) * projectileForce);
            Debug.Log("In range");
            Debug.DrawRay(ppr.lookPos.position, ppr.mainCamera.forward * hit.distance, Color.blue, 10f);
            Debug.DrawRay(ppr.shootPoint.position, (hit.point - ppr.shootPoint.position) * Vector3.Distance(hit.point, ppr.shootPoint.position), Color.black, 10f);
        }
        else
        {
            clonedProjectile.AddForce(((ppr.lookPos.position + ppr.mainCamera.forward * (aimRange.value + extraRange)) - ppr.shootPoint.position) * projectileForce);
            Debug.Log("Out of range");
            Debug.DrawRay(ppr.lookPos.position, ppr.mainCamera.forward * (aimRange.value + extraRange), Color.blue, 10f);
            Debug.DrawRay(ppr.shootPoint.position, ((ppr.lookPos.position + ppr.mainCamera.forward * (aimRange.value + extraRange)) - ppr.shootPoint.position).normalized * (aimRange.value + extraRange), Color.black, 10f);
        }
    }
}
