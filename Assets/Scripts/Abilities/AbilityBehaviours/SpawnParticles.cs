﻿using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Ability Behaviour/Spawn Particles")]
public class SpawnParticles : AbilityBehaviour
{
    public ParticleSystem particle;

    public override void Execute(PlayerPositionReference ppr, int abilityLevel)
    {
        ParticleSystem clonedParticle = Instantiate(this.particle, ppr.centrePoint.position, particle.transform.rotation, ppr.centrePoint);
    }
}
