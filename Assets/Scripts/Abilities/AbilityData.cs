﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Ability Data")]
public class AbilityData : ScriptableObject
{
    [Header("Database ID (Remember to add in Database SOs)")]
    public string dataID;
    [Tooltip("Ability ID for animator usage")]
    public int abilityID;
    [Header("Ability Data")]
    public string abilityName;
    public int requiredLevel;
    public float abilityCooldown;
    public Sprite abilityIcon; // USE AssetReference for Sprite and GameObject to minimize load time
    public List<AbilityBehaviour> abilityBehaviours = new List<AbilityBehaviour>();

    // Attributes with damage value, elemental type, etc.
    // particle system or VFX if executed
    // public float abilityDuration

    public void Execute(PlayerPositionReference ppr, int abilityLevel)
    {
        foreach (AbilityBehaviour abilityBehaviour in abilityBehaviours)
        {
            abilityBehaviour.Execute(ppr, abilityLevel);
        }
    }
    // public void Terminate()
    public float LevelUp(int SkillLevel)
    {
        float x = 0f;
        switch (SkillLevel)
        {
            case 1:
                x = 0f;
                break;
            case 2:
                x = 60f;
                break;
        }
        return x;
    }
}
