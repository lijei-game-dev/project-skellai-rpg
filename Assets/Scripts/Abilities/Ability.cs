﻿[System.Serializable]
public class Ability
{
    public AbilityData abilityData;
    public string abilityDataID;
    public int abilityLevel;
    public float abilityCooldown;
    private float cooldownLeft;
    private bool abilityReadyToExecute;

    public float CooldownLeft { get { return cooldownLeft; } }
    public bool AbilityReadyToExecute { get { return abilityReadyToExecute; } }

    public Ability(AbilityData abilityData)
    {
        this.abilityData = abilityData;
        if (abilityData != null)
        {
            this.abilityDataID = abilityData.dataID;
            this.abilityLevel = 1;
            this.abilityCooldown = abilityData.abilityCooldown;
            this.cooldownLeft = 0f;
            this.abilityReadyToExecute = true;
        }
        else
        {
            this.abilityReadyToExecute = false;
        }
    }

    public void Execute(PlayerPositionReference ppr)
    {
        if (this.abilityData != null)
        {
            this.abilityData.Execute(ppr, abilityLevel);
            if (HasCooldown())
            {
                cooldownLeft = abilityCooldown;
                abilityReadyToExecute = false;
            }
        }
    }

    // cooldownLeft will be decremented by the parameter float value
    public bool Cooldown(float time)
    {
        cooldownLeft -= time;
        if (cooldownLeft <= 0f)
        {
            cooldownLeft = 0f;
            abilityReadyToExecute = true;
            return false;
        }
        return true;
    }

    public bool OnCooldown()
    {
        return cooldownLeft > 0f;
    }

    public bool HasCooldown()
    {
        return abilityCooldown > 0f;
    }
}
