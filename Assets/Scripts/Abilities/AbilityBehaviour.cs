﻿using UnityEngine;

public abstract class AbilityBehaviour : ScriptableObject
{
    public abstract void Execute(PlayerPositionReference ppr, int abilityLevel);
}
