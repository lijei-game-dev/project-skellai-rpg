﻿using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public abstract class Interactable : MonoBehaviour
{
    public float interactionRadius = 4f;
    public abstract void Interact();
    public abstract InteractionType GetInteractionType();

    public virtual void Start()
    {
        GetComponent<SphereCollider>().radius = interactionRadius;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Debug.Log("ENTER");
            PlayerManager.Instance.AddInteraction(this);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Debug.Log("EXIT");
            PlayerManager.Instance.RemoveInteraction(this);
        }
    }
}
