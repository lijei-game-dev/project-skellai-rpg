﻿public enum InteractionType { PickUpItem };

public class Interaction
{
    public Interactable interactable;
    public InteractionType interactionType;

    public Interaction(Interactable interactable)
    {
        this.interactable = interactable;
        this.interactionType = interactable.GetInteractionType();
    }
}
