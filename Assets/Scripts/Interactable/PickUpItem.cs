﻿using System.Collections;
using UnityEngine;

public class PickUpItem : Interactable
{
    public ScriptableObject itemData;
    [Tooltip("Seconds")]
    public float selfDestructTime = 300f;

    public override void Start()
    {
        base.Start();
        StartCoroutine("Destroy");
    }

    bool PickUp()
    {
        if (GetItemDataType() == typeof(EquipmentData))
        {
            return PickUpEquipment();
        }
        // ADD MORE ITEMDATATYPE HERE
        return false;
    }

    System.Type GetItemDataType()
    {
        return itemData.GetType();
    }

    bool PickUpEquipment()
    {
        Equipment equipment = new Equipment(itemData as EquipmentData);

        if (PlayerManager.Instance.AddEquipment(equipment))
        {
            Destroy(this.gameObject);
            Debug.Log("ITEM DESTROYED AFTER PICKED UP");
            return true;
        }
        return false;
    }

    public override void Interact()
    {
        if (PickUp())
        {
            PlayerManager.Instance.RemoveInteraction(this);
        }
    }

    public override InteractionType GetInteractionType()
    {
        return InteractionType.PickUpItem;
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(selfDestructTime);
        PlayerManager.Instance.RemoveInteraction(this);
        Destroy(this.gameObject);
    }
}
