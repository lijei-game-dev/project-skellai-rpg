﻿using Cinemachine;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(PlayerPositionReference))]
public class PlayerController : MonoBehaviour
{
    public LayerMask groundMask;
    public FloatReference gravity;
    public FloatReference groundDistance;
    public FloatReference jumpHeight;
    public FloatReference moveSpeed;
    public FloatReference idleTimeout;
    public FloatReference turnSmoothTime;
    public GameObject inventoryUI;
    public GameObject playerInfoUI;
    public GameObject abilityInfoUI;

    protected CharacterController characterController;
    protected Animator animator;
    protected PlayerInput playerInput;
    protected PlayerPositionReference ppr;

    protected float turnSmoothVelocity;
    protected float forwardSpeed;
    protected float horizontalSpeed;
    protected float verticalSpeed;
    protected Quaternion targetRotation;
    protected Vector3 targetDirection;
    protected Vector3 targetMovement;
    protected Vector3 lookPosPivotRotation = Vector3.zero;
    protected bool isGrounded;
    protected bool inCombatMode = false;
    protected float idleTimer;
    protected float cameraFOV = MAX_FOV;

    const float GROUND_STICKING_GRAVITY_PROPORTION = 0.3f;
    const float MAX_FOV = 90f;
    const float MIN_FOV = 15f;
    const float FOV_INTERVAL = 7.5f;
    const float FOV_SPEED_PROPORTION = 18f;

    readonly int hashForwardSpeed = Animator.StringToHash("ForwardSpeed");
    readonly int hashHorizontalSpeed = Animator.StringToHash("HorizontalSpeed");
    readonly int hashVerticalSpeed = Animator.StringToHash("VerticalSpeed");
    readonly int hashGrounded = Animator.StringToHash("Grounded");
    readonly int hashInputDetected = Animator.StringToHash("InputDetected");
    readonly int hashCombatMode = Animator.StringToHash("CombatMode");
    readonly int hashIdleTimeout = Animator.StringToHash("IdleTimeout");
    readonly int hashMeleeAttack = Animator.StringToHash("MeleeAttack");
    readonly int hashInteract = Animator.StringToHash("Interact");
    readonly int hashInteractionType = Animator.StringToHash("InteractionType");

    void Awake()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        playerInput = GetComponent<PlayerInput>();
        ppr = GetComponent<PlayerPositionReference>();
        Cursor.lockState = CursorLockMode.Locked;
        // ppr.TPSCamera.GetComponent<CinemachineFreeLook>().m_Lens.FieldOfView = cameraFOV;
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject()) { return; }
        CalculateLookPosPosition();
        CalculateForwardMovement();
        CalculateVerticalMovement();
        CalculateTargetRotation();
        InteractionFunction();
        InventoryDisplayFunction();
        PlayerInfoDisplayFunction();
        AbilityInfoDisplayFunction();
        CursorDisplayFunction();
        ToggleCombatModeFunction();
        ZoomCameraFunction();
        TimeoutToIdle();
        if (inCombatMode)
        {
            AttackFunction();
            ExecuteAbilityFunction();
        }
    }

    void CalculateLookPosPosition()
    {
        lookPosPivotRotation.y = ppr.mainCamera.eulerAngles.y;
        ppr.lookPosRotationPivot.eulerAngles = lookPosPivotRotation;
    }

    void CalculateTargetRotation()
    {
        if (inCombatMode)
        {
            targetRotation = Quaternion.Euler(0f, ppr.mainCamera.eulerAngles.y, 0f);
        }
        else
        {
            if (targetDirection.magnitude >= 0.1f)
            {
                float targetAngle = Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg + ppr.mainCamera.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime.value);
                targetRotation = Quaternion.Euler(0f, angle, 0f);
            }
        }
    }

    void CalculateForwardMovement()
    {
        targetDirection.Set(playerInput.MovementInput.x, 0f, playerInput.MovementInput.y);
        targetDirection = targetDirection.normalized;
        // If player is moving and not in attack mode (change the second condition into something like inputDetected for actions that disable movement)
        if (targetDirection.magnitude >= 0.1f && !animator.GetCurrentAnimatorStateInfo(0).shortNameHash.Equals(hashMeleeAttack))
        {
            float targetAngle = Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg + ppr.mainCamera.eulerAngles.y;
            targetMovement = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward * moveSpeed.value * Time.deltaTime;

            if (inCombatMode)
            {
                // HORIZONTAL MOVEMENT
                if (playerInput.MovementInput.x == 1) // Moving RIGHT
                {
                    horizontalSpeed = moveSpeed.value;
                }
                else if (playerInput.MovementInput.x == -1) // Moving LEFT
                {
                    horizontalSpeed = -moveSpeed.value;
                }
                else
                {
                    horizontalSpeed = Mathf.MoveTowards(horizontalSpeed, 0f, moveSpeed.value * 3 * Time.deltaTime);
                }

                // FORWARD MOVEMENT
                if (playerInput.MovementInput.y == 1) // Moving FORWARD
                {
                    forwardSpeed = moveSpeed.value;
                }
                else if (playerInput.MovementInput.y == -1) // Moving BACKWARD
                {
                    forwardSpeed = -moveSpeed.value;
                }
                else
                {
                    forwardSpeed = Mathf.MoveTowards(forwardSpeed, 0f, moveSpeed.value * 3 * Time.deltaTime);
                }
            }
            else
            {
                forwardSpeed = moveSpeed.value;
            }
        }
        else
        {
            targetMovement = Vector3.zero;
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, 0f, moveSpeed.value * 3 * Time.deltaTime); // REMOVE THIS LINE BY ADDING STOP RUNNING ANIMATION (like a brake)
            horizontalSpeed = Mathf.MoveTowards(horizontalSpeed, 0f, moveSpeed.value * 3 * Time.deltaTime);
        }
        animator.SetFloat(hashForwardSpeed, forwardSpeed);
        animator.SetFloat(hashHorizontalSpeed, horizontalSpeed);
    }

    // Gravity
    void CalculateVerticalMovement()
    {
        if (isGrounded)
        {
            if (verticalSpeed < gravity.value * GROUND_STICKING_GRAVITY_PROPORTION)
            {
                verticalSpeed = gravity.value * GROUND_STICKING_GRAVITY_PROPORTION;
            }

            if (playerInput.JumpInput)
            {
                verticalSpeed = Mathf.Sqrt(jumpHeight.value * -2f * gravity.value);
            }
        }
        else
        {
            verticalSpeed += gravity.value * Time.deltaTime;
        }
        targetMovement += Vector3.up * verticalSpeed * Time.deltaTime;
        animator.SetFloat(hashVerticalSpeed, verticalSpeed);
    }

    void OnAnimatorMove()
    {
        transform.rotation = targetRotation;
        characterController.Move(targetMovement);

        RaycastHit hit;
        Ray ray = new Ray(ppr.groundCheck.position + Vector3.up * 0.5f, -Vector3.up);
        isGrounded = Physics.Raycast(ray, out hit, groundDistance.value + 0.5f, groundMask, QueryTriggerInteraction.Ignore) ? true : false;
        animator.SetBool(hashGrounded, isGrounded);
    }

    void TimeoutToIdle()
    {
        bool movementInput = playerInput.MovementInput.sqrMagnitude > 0;
        bool inputDetected = movementInput || playerInput.JumpInput || playerInput.AttackInput;
        if (isGrounded && !inputDetected && !inCombatMode)
        {
            idleTimer += Time.deltaTime;
            if (idleTimer > idleTimeout.value)
            {
                idleTimer = 0f;
                animator.SetTrigger(hashIdleTimeout);
            }
        }
        else
        {
            idleTimer = 0f;
            animator.ResetTrigger(hashIdleTimeout);
        }
        animator.SetBool(hashInputDetected, inputDetected);
    }

    void AttackFunction()
    {
        animator.ResetTrigger(hashMeleeAttack);
        if (playerInput.AttackInput)
        {
            animator.SetTrigger(hashMeleeAttack);
        }
    }

    void InteractionFunction()
    {
        if (playerInput.InteractionInput)
        {
            if (PlayerManager.Instance.interactions.Count > 0)
            {
                animator.SetTrigger(hashInteract);
                animator.SetInteger(hashInteractionType, PlayerManager.Instance.ExecuteFirstInteraction());
            }
        }
    }

    void InventoryDisplayFunction()
    {
        if (inventoryUI != null)
        {
            if (playerInput.DisplayInventoryInput)
            {
                Debug.Log("I");
                inventoryUI.SetActive(!inventoryUI.activeSelf);
                inventoryUI.GetComponentInChildren<EquipmentInventoryUI>().UpdateEquipmentInventoryUI();
            }
        }
    }

    void PlayerInfoDisplayFunction()
    {
        if (playerInfoUI != null)
        {
            if (playerInput.DisplayPlayerInfoInput)
            {
                Debug.Log("P");
                playerInfoUI.SetActive(!playerInfoUI.activeSelf);
                playerInfoUI.GetComponentInChildren<PlayerEquipmentInfoUI>().UpdatePlayerEquipmentInfoUI();
            }
        }
    }

    void AbilityInfoDisplayFunction()
    {
        if (abilityInfoUI != null)
        {
            if (playerInput.DisplayAbilityInfoInput)
            {
                Debug.Log("K");
                abilityInfoUI.SetActive(!abilityInfoUI.activeSelf);
            }
        }
    }

    void CursorDisplayFunction()
    {
        if (playerInput.DisplayCursorInput)
        {
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else if (Cursor.lockState == CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            Debug.Log(Cursor.lockState);
        }
    }

    void ToggleCombatModeFunction()
    {
        if (playerInput.ToggleCombatModeInput)
        {
            inCombatMode = !inCombatMode;
            animator.SetBool(hashCombatMode, inCombatMode);
            Debug.Log("In Combat Mode: " + inCombatMode);
        }
    }

    void ZoomCameraFunction()
    {
        if (playerInput.MouseScrollWheelInput > 0f) // Zoom In
        {
            Debug.Log("Zoom In");
            if (cameraFOV > MIN_FOV)
            {
                cameraFOV = cameraFOV - FOV_INTERVAL;
            }
        }
        else if (playerInput.MouseScrollWheelInput < 0f) // Zoom Out
        {
            Debug.Log("Zoom Out");
            if (cameraFOV < MAX_FOV)
            {
                cameraFOV = cameraFOV + FOV_INTERVAL;
            }
        }

        if (cameraFOV != ppr.TPSCamera.GetComponent<CinemachineFreeLook>().m_Lens.FieldOfView)
        {
            ppr.TPSCamera.GetComponent<CinemachineFreeLook>().m_Lens.FieldOfView = Mathf.MoveTowards(ppr.TPSCamera.GetComponent<CinemachineFreeLook>().m_Lens.FieldOfView, cameraFOV, FOV_INTERVAL * FOV_SPEED_PROPORTION * Time.deltaTime);
        }
    }

    void ExecuteAbilityFunction()
    {
        if (playerInput.ExecuteAbilityInput01)
        {
            Debug.Log("1");
            AbilityManager.Instance.ExecuteAbility(1, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput02)
        {
            Debug.Log("2");
            AbilityManager.Instance.ExecuteAbility(2, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput03)
        {
            Debug.Log("3");
            AbilityManager.Instance.ExecuteAbility(3, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput04)
        {
            Debug.Log("4");
            AbilityManager.Instance.ExecuteAbility(4, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput05)
        {
            Debug.Log("5");
            AbilityManager.Instance.ExecuteAbility(5, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput06)
        {
            Debug.Log("6");
            AbilityManager.Instance.ExecuteAbility(6, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput07)
        {
            Debug.Log("7");
            AbilityManager.Instance.ExecuteAbility(7, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput08)
        {
            Debug.Log("8");
            AbilityManager.Instance.ExecuteAbility(8, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput09)
        {
            Debug.Log("9");
            AbilityManager.Instance.ExecuteAbility(9, GetComponent<PlayerPositionReference>());
        }
        if (playerInput.ExecuteAbilityInput10)
        {
            Debug.Log("10");
            AbilityManager.Instance.ExecuteAbility(10, GetComponent<PlayerPositionReference>());
        }
    }
}
