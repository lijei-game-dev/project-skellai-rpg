﻿using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    protected Vector2 movement;
    protected float mouseScrollWheel;
    protected bool jump;
    protected bool attack;
    protected bool interaction;
    protected bool displayCursor;
    protected bool displayInventory;
    protected bool displayPlayerInfo;
    protected bool displayAbilityInfo;
    protected bool toggleCombatMode;
    protected bool executeAbility01;
    protected bool executeAbility02;
    protected bool executeAbility03;
    protected bool executeAbility04;
    protected bool executeAbility05;
    protected bool executeAbility06;
    protected bool executeAbility07;
    protected bool executeAbility08;
    protected bool executeAbility09;
    protected bool executeAbility10;

    public Vector2 MovementInput { get { return movement; } }
    public float MouseScrollWheelInput { get { return mouseScrollWheel; } }
    public bool JumpInput { get { return jump; } }
    public bool AttackInput { get { return attack; } }
    public bool InteractionInput { get { return interaction; } }
    public bool DisplayCursorInput { get { return displayCursor; } }
    public bool DisplayInventoryInput { get { return displayInventory; } }
    public bool DisplayPlayerInfoInput { get { return displayPlayerInfo; } }
    public bool DisplayAbilityInfoInput { get { return displayAbilityInfo; } }
    public bool ToggleCombatModeInput { get { return toggleCombatMode; } }
    public bool ExecuteAbilityInput01 { get { return executeAbility01; } }
    public bool ExecuteAbilityInput02 { get { return executeAbility02; } }
    public bool ExecuteAbilityInput03 { get { return executeAbility03; } }
    public bool ExecuteAbilityInput04 { get { return executeAbility04; } }
    public bool ExecuteAbilityInput05 { get { return executeAbility05; } }
    public bool ExecuteAbilityInput06 { get { return executeAbility06; } }
    public bool ExecuteAbilityInput07 { get { return executeAbility07; } }
    public bool ExecuteAbilityInput08 { get { return executeAbility08; } }
    public bool ExecuteAbilityInput09 { get { return executeAbility09; } }
    public bool ExecuteAbilityInput10 { get { return executeAbility10; } }

    void Update()
    {
        movement.Set(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        mouseScrollWheel = Input.GetAxisRaw("Mouse ScrollWheel");
        jump = Input.GetButtonDown("Jump");
        attack = Input.GetKeyDown(KeyCode.R);
        interaction = Input.GetKeyDown(KeyCode.F);
        displayCursor = Input.GetKeyDown(KeyCode.LeftControl);
        displayInventory = Input.GetKeyDown(KeyCode.I);
        displayPlayerInfo = Input.GetKeyDown(KeyCode.P);
        displayAbilityInfo = Input.GetKeyDown(KeyCode.K);
        toggleCombatMode = Input.GetKeyDown(KeyCode.Tab);
        executeAbility01 = Input.GetKeyDown(KeyCode.Alpha1);
        executeAbility02 = Input.GetKeyDown(KeyCode.Alpha2);
        executeAbility03 = Input.GetKeyDown(KeyCode.Alpha3);
        executeAbility04 = Input.GetKeyDown(KeyCode.Alpha4);
        executeAbility05 = Input.GetKeyDown(KeyCode.Alpha5);
        executeAbility06 = Input.GetKeyDown(KeyCode.Alpha6);
        executeAbility07 = Input.GetKeyDown(KeyCode.Alpha7);
        executeAbility08 = Input.GetKeyDown(KeyCode.Alpha8);
        executeAbility09 = Input.GetKeyDown(KeyCode.Alpha9);
        executeAbility10 = Input.GetKeyDown(KeyCode.Alpha0);
    }
}
