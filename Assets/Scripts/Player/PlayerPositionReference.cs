﻿using UnityEngine;

public class PlayerPositionReference : MonoBehaviour
{
    public Transform parent;
    public Transform centrePoint;
    public Transform lookPosRotationPivot;
    public Transform lookPos;
    public Transform groundCheck;
    public Transform shootPoint;
    public Transform mainCamera;
    public Transform TPSCamera;
}
